import 'package:flutter/material.dart';

abstract class AppColors {
  static Color get black => Colors.black;
  static Color get white => Colors.white;
  static Color get red => Colors.red;
  static Color get primaryColorLight => Colors.grey;
  static Color get primary => const Color(0xff5458F7);
  static Color get background => const Color(0xFF1C1C1D);
  static Color get grey => const Color(0xFF909090);
  static Color get secondGrey => const Color(0xFFC6C6C6);
  static Color get trirdGrey => const Color(0xFF898989);
  static Color get dividingLine => const Color(0xFF47484A);
  static Color get lightBlue => const Color(0xFFB2DFFF);
  static Color get lightGrey => const Color(0xFF3A3A3C);
  static Color get darkBlack => const Color(0xFF060606);
  static Color get darkGrey => const Color(0xFF262628);
  static Color get mediumGrey => const Color(0xFF313131);
  static Color get lightBlack => const Color(0xFF434343);
  static Color get dividerColorLight => const Color(0xFFE0E0E0);
}
